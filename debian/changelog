octave-doctest (0.8.0-1) unstable; urgency=medium

  * New upstream version 0.8.0
  * d/copyright: Reflect upstream changes
  * d/control: Bump Standards-Version to 4.6.2 (no changes needed)

 -- Rafael Laboissière <rafael@debian.org>  Sun, 08 Jan 2023 06:12:58 -0300

octave-doctest (0.7.0-5) unstable; urgency=medium

  * d/copyright:
    + Accentuate my family name
    + Update Copyright years for debian/* files
  * d/watch:
    + Bump to format version 4
    + Adjust for new URL at gnu-octave.github.io
  * d/control: Bump Standards-Version to 4.6.1 (no changes needed)
  * Build-depend on dh-sequence-octave
    + d/control: Ditto
    + d/rules: Drop the --with=octave option from dh call
  * d/s/lintian-override: Override Lintian warning
    debian-watch-lacks-sourceforge-redirector

 -- Rafael Laboissière <rafael@debian.org>  Mon, 05 Dec 2022 05:18:36 -0300

octave-doctest (0.7.0-4) unstable; urgency=medium

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation lines.
  * Set upstream metadata fields: Archive, Repository.

  [ Rafael Laboissière ]
  * d/u/metadata: Change all URLs from Savannah to GitHub

 -- Rafael Laboissière <rafael@debian.org>  Tue, 27 Oct 2020 12:56:53 -0300

octave-doctest (0.7.0-3) unstable; urgency=medium

  * d/control:
    + Bump dependency on dh-octave to >= 0.7.1
      This allows the injection of the virtual package octave-abi-N into
      the package's list of dependencies.
    + Bump Standards-Version to 4.5.0 (no changes needed)
    + Bump debhelper compatibility level to 13
  * d/u/metadata: New file

 -- Rafael Laboissière <rafael@debian.org>  Tue, 28 Jul 2020 05:42:27 -0300

octave-doctest (0.7.0-2) unstable; urgency=medium

  * Upload to unstable
  * d/copyright: Add copyright and license information for the AppStream file
  * d/control: Bump Standards-Version to 4.4.0 (no changes needed)

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 10 Jul 2019 05:14:04 -0300

octave-doctest (0.7.0-1) experimental; urgency=medium

  * New upstream version 0.7.0
  * d/copyright: Reflect upstream changes

 -- Rafael Laboissiere <rafael@debian.org>  Mon, 06 May 2019 10:18:38 -0300

octave-doctest (0.6.1-2) unstable; urgency=medium

  * d/control:
    + Add Rules-Requires-Root: no
    + Bump Standards-Version to 4.3.0
    + Bump to debhelper compat level 12
  * Build-depend on debhelper-compat instead of using d/compat

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 02 Jan 2019 22:55:16 -0200

octave-doctest (0.6.1-1) unstable; urgency=low

  * Initial release (closes: #898313)

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 30 Jun 2018 08:30:38 -0300
